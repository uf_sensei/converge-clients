#!/usr/bin/python
import sys 
import os 
import json 
import sqlite3
import socket 
import ssl 
import hashlib
import base64
import time
import pycurl
from StringIO import StringIO
from subprocess import call
import tempfile


# Global Vars
config = {} 
notaries = []
db = None 
userdb = None 

debug = True

# Entry/exit point. Here we go!
def main():
    # Initialize
    init()

    # Bail if we have wrong number of args
    if len(sys.argv) < 3: exit(1)

    host = sys.argv[1]
    port = sys.argv[2]
    if len(sys.argv) == 4:
        fingerprint = sys.argv[3].upper()
    else:
        fingerprint = get_fingerprint(host, port)

    # Perform lookup
    if debug: print "[+] Notary lookup: %s:%s - %s" %(host, port, fingerprint)

    result = notarize(host, port, fingerprint)

    # We're golden
    if result:
        exit(0)

    # We're fucked
    exit(1)


# Set up global vars TODO parse command line options
def init(): 
    global config, db, userdb, notaries

    config_path = '/usr/local/etc/converge/converge.config'

    # Load config
    with open(config_path, 'r') as data:
        config = json.load(data)

    # Set up the dbs for caching...
    db = sqlite3.connect(config['db'])
    userdb = sqlite3.connect(os.environ['HOME'] +'/'+ config['userdb'])

    # Read our enabled notaries
    for f in os.listdir(config['notaries-dir']):
        if f == '.' or f == '..': continue

        bundle_path = config['notaries-dir'] + f

        with open(bundle_path, 'r') as data:
            notaries.append(json.load(data))


def get_fingerprint(host, port):
    # Get out x509 cert
    cert = ssl.get_server_certificate((host, int(port))) 
    cert = ssl.PEM_cert_to_DER_cert(cert)

    # Add colons as per convergence spec, also it must be upper case for
    # some reason
    fingerprint = hashlib.sha1(cert).hexdigest()
    fingerprint = ':'.join([fingerprint[i:i+2] 
      for i in range(0, len(fingerprint), 2)]).upper()
    return fingerprint


def cache_check(host, port, fingerprint):
    global db, userdb
    return (_cache_check(host, port, fingerprint, db) or
            _cache_check(host, port, fingerprint, userdb))


def _cache_check(host, port, fingerprint, db):
    with db:
        cur = db.cursor()
        stmt = ('SELECT COUNT(*) FROM fingerprints WHERE location = ? '
               'AND fingerprint = ? AND timestamp > ?')
 
        timestamp = int(time.time())-60*60*25*7
        cur.execute(stmt, ('%s:%s' %(host, port), fingerprint, timestamp))
        data = cur.fetchone()
        return data[0]


def cache_update(result, host, port):
    global db, userdb, config
    _cache_update(result, host, port, userdb)

    # If we can update the global cache, do it
    if os.access(config['db'], os.W_OK):
        _cache_update(result, host, port, db)


def _cache_update(result, host, port, db):
    stmt = ('INSERT OR IGNORE INTO fingerprints (location, fingerprint)'
           'VALUES (?, ?)')
    stmt2 = ('UPDATE fingerprints SET timestamp = ? WHERE location = ? '
            'AND fingerprint = ?')
    for r in result.values():
        if r[0]:
            data = r[1]['fingerprintList']

            for d in data:
                    fprint = d['fingerprint']
                    with db:
                        cur = db.cursor()
                        
                        cur.execute(stmt, ('%s:%s' %(host, port), fprint))
                        
                        cur.execute(stmt2, (int(time.time()), '%s:%s' 
                            %(host, port), fprint))


def notarize(host, port, fingerprint):
    # First, check our local cache
    result = cache_check(host, port, fingerprint)

    # Cache hit, we're happy
    if result:
        if debug: print '[+] Cache hit: %s %s - %s' %(host, port, fingerprint)
        return True

    # Cache miss, perform remote check      
    result = remote_check(host, port, fingerprint)

    # Update our caches
    cache_update(result, host, port)

    # Count success/failures    
    good = bad = 0
    for value in result.values():
        if value[0]:
            good += 1
        else:
            bad += 1

    if debug: print '[+] Notary hits: %d\n[+] Notary misses: %d' %(good, bad)

    if config['notary-agreement'] == 'consensus':
        if bad: return False

    elif config['notary-agreement'] == 'majority':
        if good > bad: return True

    else:
        return False


def remote_check(remote_host, remote_port, fingerprint):
    global notaries
    requests = []
    results = {}
    certs = {}
    curl_results = []

    for notary in notaries:
        hosts = notary['hosts']
        
        # Loop over all hosts in a notary
        for host in hosts:
            # Build the notary request url
            url = 'https://%s:%s/target/%s+%s' %(host['host'], 
                    str(host['ssl_port']), remote_host, str(remote_port))

            cert = host['certificate']
            certs[url] = cert
    
    multi_handle = pycurl.CurlMulti()
    for url in certs.keys():
        response = StringIO()
        curl_handle = pycurl.Curl()
        curl_handle.setopt(pycurl.URL, url.encode('ascii', 'ignore'))
        curl_handle.setopt(pycurl.POST, 1)
        curl_handle.setopt(pycurl.POSTFIELDS, 'fingerprint=%s' %fingerprint)
        curl_handle.setopt(pycurl.WRITEFUNCTION, response.write)
        curl_handle.setopt(pycurl.SSL_VERIFYPEER, 0)
        #curl_handle.setopt(pycurl.VERBOSE, 1)

        multi_handle.add_handle(curl_handle)
        req = (curl_handle, response)
        requests.append(req)
    
    SELECT_TIMEOUT = 1.0
    num_handles = len(requests)
    while num_handles:
        ret = multi_handle.select(SELECT_TIMEOUT)
        if ret == -1:
            continue
    
        while 1:
            ret, num_handles = multi_handle.perform()
            if ret != pycurl.E_CALL_MULTI_PERFORM: break

    for (key, req) in enumerate(requests):
        http_code = req[0].getinfo(pycurl.HTTP_CODE)
        url = req[0].getinfo(pycurl.EFFECTIVE_URL)   

        if http_code != 200:
            markit_zero(results, url)
            continue
        curl_results.append({'url' :url, 'cert': certs[url], 'data' :req[1].getvalue()})
    
    multi_handle.close()
        
    for key in curl_results:
        response = key['data']
        cert     = key['cert']
        url      = key['url']

        data = json.loads(response)
        sig  = base64.b64decode(data['signature'])
        del data['signature']
        message = json.dumps(data)
        
        #TODO find a different way that doesn't require calling openssl
        
        # Get public key from certificate
        tmp_pkey = tempfile.NamedTemporaryFile()
        tmp_cert = tempfile.NamedTemporaryFile()
        tmp_cert.write(cert)
        tmp_cert.seek(0)

        os.popen('openssl x509 -in %s -pubkey -noout > %s'
            %(tmp_cert.name, tmp_pkey.name))
        
        tmp_sig = tempfile.NamedTemporaryFile()
        tmp_sig.write(sig)
        tmp_sig.seek(0)

        tmp_data = tempfile.NamedTemporaryFile()
        tmp_data.write(message)
        tmp_data.seek(0)

        # Verify the signature
        verify = os.system('openssl dgst -sha1 -verify %s -signature %s %s > /dev/null'
            %(tmp_pkey.name, tmp_sig.name, tmp_data.name))

        tmp_pkey.close()
        tmp_sig.close()
        tmp_cert.close()
        tmp_data.close()

        if verify == 0:
            markit_good(results, url, data)
        else:
            markit_zero(results, url)
    return results


def markit_good(results, url, response):
    results[url] = [True, response]


def markit_zero(results, url):
    results[url] = [False]


if __name__ == '__main__': 
  main()

