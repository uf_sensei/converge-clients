import java.util.*;
import java.io.*;
import java.net.*;
import javax.net.ssl.*;
import java.sql.*;
import org.json.*;
import java.security.cert.*;
import java.security.SecureRandom;
import java.security.Signature;
import org.apache.commons.codec.binary.*;
import org.apache.commons.codec.digest.*;
import org.apache.commons.lang3.StringUtils;

public class JConverge {
    static Connection db;
    static Connection userdb;
    static List<JSONObject> notaries;
    static JSONObject config;

    static Boolean debug = true;

    public static void main(String[] args) {
        /* Initialize */
        try {
            init();
        } catch (Exception e) {
            if (debug)
                System.out.println("[-] Initialization failed");
            e.printStackTrace();
            System.exit(1);
        }
        /* Bail if we have wrong number of args */
        if (args.length < 2)
            System.exit(1);

        String host = args[0];
        String port = args[1];
        String fingerprint = "";
        if (args.length == 3)
            fingerprint = args[2].toUpperCase();
        else {
            try {
                fingerprint = getFingerprint(host, port);
            } catch (CertificateEncodingException | IOException e) {
                if (debug)
                    System.out.println("[-] Failed to retrieve fingerprint");
                e.printStackTrace();
                System.exit(1);
            }
        }
        /* Perform lookup */
        if (debug)
            System.out.println(String.format("[+] Notary lookup: %s:%s - %s",
                    host, port, fingerprint));
        boolean result = notarize(host, port, fingerprint);

        /* We're golden */
        if (result)
            System.exit(0);

        /* We're fucked */
        System.exit(1);
    }

    public static void init() throws Exception {
        String configPath = "/usr/local/etc/converge/converge.config";

        /* Load config */
        String data = new Scanner(new File(configPath)).useDelimiter("\\Z")
                .next();

        config = new JSONObject(data);

        /* Set up the dbs for caching... */
        Class.forName("org.sqlite.JDBC");

        db = DriverManager.getConnection(String.format("jdbc:sqlite:%s",
                config.getString("db")));

        userdb = DriverManager.getConnection(String.format("jdbc:sqlite:%s",
                config.getString("userdb")));

        /* Read our enabled notaries */
        notaries = new ArrayList<JSONObject>();
        String path = config.getString("notaries-dir");
        String files, bundlePath;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                files = listOfFiles[i].getName();
                // if (files.equals(".DS_Store")) // STUPID MAC FILE
                // continue;
                bundlePath = path + files;
                data = new Scanner(new File(bundlePath)).useDelimiter("\\Z")
                        .next();
                notaries.add(new JSONObject(data));
            }
        }
    }

    /* Performs local x509 retrieval */
    public static String getFingerprint(String host, String port)
            throws IOException, CertificateEncodingException {

        /* Open up our connection */
        String httpsUrl = String.format("https://%s:%s", host, port);
        URL url = new URL(httpsUrl);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.getResponseCode();

        /* Get out x509 cert */
        Certificate[] certs = con.getServerCertificates();
        String fingerprint = DigestUtils.sha1Hex(certs[0].getEncoded());

        /* Add colons as per convergence spec */
        fingerprint = StringUtils.join(fingerprint.split("(?<=\\G..)"), ":");
        return fingerprint.toUpperCase();
    }

    public static boolean notarize(String host, String port, String fingerprint) {
        /* First, check our local cache */
        boolean result;
        try {
            result = cacheCheck(host, port, fingerprint);

            /* Cache hit, we're happy */
            if (result) {
                if (debug)
                    System.out.println(String.format(
                            "[+] Cache hit: %s:%s - %s", host, port,
                            fingerprint));
                return true;
            }
        } catch (SQLException e) {
            if (debug)
                System.out.println("[-] Failed to check local cache");
        }
        /* Cache miss, perform remote check */
        HashMap<String, Object[]> remoteResult = remoteCheck(host, port,
                fingerprint);

        /* Update our caches */
        try {
            cacheUpdate(remoteResult, host, port);
        } catch (SQLException e) {
            if (debug)
                System.out.println("[-] Failed to update local cache");
        }

        /* Count success/failures */
        int good = 0, bad = 0;
        Collection<Object[]> values = remoteResult.values();
        for (Object[] value : values) {
            if ((boolean) value[0])
                good += 1;
            else
                bad += 1;
        }
        if (debug)
            System.out.println(String.format(
                    "[+] Notary hits: %d\n[+] Notary misses: %d", good, bad));

        if (config.getString("notary-agreement").equalsIgnoreCase("consensus")) {
            if (bad > 0)
                return false;
        } else if (config.getString("notary-agreement").equalsIgnoreCase(
                "majority")) {
            if (good > bad)
                return true;
        }
        return false;
    }

    public static boolean cacheCheck(String host, String port,
            String fingerprint) throws SQLException {
        return (_cacheCheck(host, port, fingerprint, db) > 0)
                || (_cacheCheck(host, port, fingerprint, userdb) > 0);
    }

    private static int _cacheCheck(String host, String port,
            String fingerprint, Connection db) throws SQLException {
        String timestamp = Long.toString((System.currentTimeMillis() / 1000L)
                - 60L * 60L * 25L * 7L);
        String query = "SELECT COUNT(*) FROM fingerprints WHERE location = ? "
                + "AND fingerprint = ? AND timestamp > ?";
        PreparedStatement stmt = db.prepareStatement(query);
        stmt.setString(1, String.format("%s:%s", host, port));
        stmt.setString(2, fingerprint);
        stmt.setString(3, timestamp);
        int result = stmt.executeQuery().getInt(1);
        return result;
    }

    public static void cacheUpdate(HashMap<String, Object[]> result,
            String host, String port) throws SQLException {
        _cacheUpdate(result, host, port, userdb);
        /* If we can update the global cache, do it */
        if ((new File(config.getString("db")).canWrite()))
            _cacheUpdate(result, host, port, db);
    }

    private static void _cacheUpdate(HashMap<String, Object[]> result,
            String host, String port, Connection db) throws SQLException {
        for (Object[] r : result.values()) {
            if ((boolean) r[0]) {
                JSONArray data = ((JSONObject) r[1])
                        .getJSONArray("fingerprintList");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject d = data.getJSONObject(i);
                    String fingerprint = d.getString("fingerprint");
                    String query = "INSERT OR IGNORE INTO fingerprints "
                            + "(location, fingerprint) VALUES (?, ?)";
                    PreparedStatement stmt = db.prepareStatement(query);
                    stmt.setString(1, String.format("%s:%s", host, port));
                    stmt.setString(2, fingerprint);
                    query = "UPDATE fingerprints SET timestamp = ? WHERE "
                            + "location = ?  AND fingerprint = ?";
                    stmt.execute();
                    stmt = db.prepareStatement(query);
                    stmt.setString(1,
                            Long.toString(System.currentTimeMillis() / 1000L));
                    stmt.setString(2, String.format("%s:%s", host, port));
                    stmt.setString(3, fingerprint);
                    stmt.execute();
                }
            }
        }
    }

    /* TODO implement parallel connections */
    public static HashMap<String, Object[]> remoteCheck(String remoteHost,
            String remotePort, String fingerprint) {

        HashMap<String, Object[]> results = new HashMap<String, Object[]>();

        /* Note we don't check SSL validity upon request */
        disableCertificateValidation();

        /* Loop over all notaries */
        for (JSONObject notary : notaries) {
            JSONArray hosts = notary.getJSONArray("hosts");

            /* Loop over all hosts in a notary */
            for (int i = 0; i < hosts.length(); i++) {
                JSONObject host = hosts.getJSONObject(i);

                /* Build the notary request url */
                String url = String.format("https://%s:%s/target/%s+%s",
                        host.getString("host"), host.getInt("ssl_port"),
                        remoteHost, remotePort);

                /* Get the public key out of the cert */
                Certificate cert = null;
                try {
                    cert = CertificateFactory.getInstance("X509")
                            .generateCertificate(
                                    new ByteArrayInputStream(((String) host
                                            .get("certificate")).getBytes()));
                } catch (CertificateException e1) {
                    continue;
                }

                String response;
                try {
                    String urlParameters = "fingerprint=" + fingerprint;
                    URL request = new URL(url);
                    HttpsURLConnection connection = (HttpsURLConnection) request
                            .openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setInstanceFollowRedirects(false);
                    connection.setUseCaches(false);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type",
                            "application/x-www-form-urlencoded");
                    connection.setRequestProperty("charset", "utf-8");
                    connection
                            .setRequestProperty(
                                    "Content-Length",
                                    ""
                                            + Integer.toString(urlParameters
                                                    .getBytes().length));
                    connection.connect();
                    DataOutputStream wr = new DataOutputStream(
                            connection.getOutputStream());
                    wr.writeBytes(urlParameters);
                    wr.flush();
                    wr.close();

                    /*
                     * Weird bug: google.com+443 http response (409 conflict)
                     * url works fine with other clients
                     */
                    if (connection.getResponseCode() != 200) {
                        markitZero(results, url);
                        continue;
                    }
                    String str;
                    response = "";
                    InputStream input = connection.getInputStream();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(input));
                    while ((str = br.readLine()) != null) {
                        response += str;
                    }
                    input.close();
                    connection.disconnect();

                } catch (IOException e) {
                    markitZero(results, url);
                    continue;
                }

                /* Rip out the signature */
                JSONObject data = new JSONObject(response);
                byte[] sigBytes = Base64.decodeBase64(data
                        .getString("signature"));
                // data.remove("signature");
                // String message = data.toString(); FAILS: order is messed up
                // message = message.replace(",", ", ").replace("\":", "\": ");
                String message = response.substring(0,
                        response.indexOf(", \"signature\""))
                        + "}";

                /* Check the sig to make up for not check the cert on connect */
                Signature sig;
                try {
                    sig = Signature.getInstance("SHA1withRSA");

                    sig.initVerify(cert);
                    sig.update(message.getBytes());
                    if (sig.verify(sigBytes))
                        markitGood(results, url, new JSONObject(response));
                    else
                        markitZero(results, url);
                } catch (Exception e) {
                    markitZero(results, url);
                    continue;
                }
            }
        }
        return results;
    }

    public static void markitGood(HashMap<String, Object[]> results,
            String url, JSONObject response) {
        Object[] value = { true, response };
        results.put(url, value);
    }

    public static void markitZero(HashMap<String, Object[]> results, String url) {
        Object[] value = { false };
        results.put(url, value);
    }

    /* Source: https://gist.github.com/henrik242/1510165 */
    public static void disableCertificateValidation() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] certs,
                    String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs,
                    String authType) {
            }
        } };

        // Ignore differences between given hostname and certificate hostname
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection
                    .setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(hv);
        } catch (Exception e) {
        }
    }
}
