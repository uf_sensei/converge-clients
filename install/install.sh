#!/bin/bash

PREFIX=/usr/local
INSTDIR=$PREFIX/etc/converge
DBDIR=/var/lib/converge

mkdir -p $DBDIR
mkdir $HOME/.converge
mkdir $HOME/bin

mkdir -p $INSTDIR/notaries-available
mkdir -p $INSTDIR/notaries-enabled

cp converge.config $INSTDIR
cp *.notary $INSTDIR/notaries-available

for n in $(ls $INSTDIR/notaries-available)
do
	ln -s $INSTDIR/notaries-available/$n $INSTDIR/notaries-enabled/$n
done


sqlite3 $DBDIR/converge.db < converge.sql
sqlite3 $DBDIR/cache.db < cache.sql

sqlite3 $HOME/.converge/converge.db < converge.sql
sqlite3 $HOME/.converge/hook.db < hook.sql
sqlite3 $HOME/.converge/cache.db < cache.sql

chown -R $SUDO_USER:$SUDO_USER $HOME/.converge
chown $SUDO_USER:$SUDO_USER $HOME/bin/cconverge
